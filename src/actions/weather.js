import axios from 'axios';
import { auth as authService } from '../service';
import { logout as logoutAction } from './auth';
import {
  CITY_WEATHER_FETCHED,
  CITY_WEATHER_LOADING,
  CITY_WEATHER_ERROR,
  CITY_WEATHER_RESET
} from '../constants';

const { REACT_APP_API_BASE: API_BASE } = process.env;

export const fetch = cityId => {
  return dispatch => {
    dispatch({ type: CITY_WEATHER_LOADING });

    axios
      .get(`${API_BASE}/cities/${cityId}`, {
        headers: { Authorization: `Bearer ${authService.getToken()}` }
      })
      .then(response => {
        dispatch({ type: CITY_WEATHER_FETCHED, weather: response.data });
      })
      .catch(err => {
        if (err.response.status === 401) {
          return dispatch(logoutAction()); // expired token case
        }

        const msg = 'Something went wrong, please try again later';
        dispatch({ type: CITY_WEATHER_ERROR, error: msg });
      });
  };
};

export const reset = () => {
  return { type: CITY_WEATHER_RESET };
};
