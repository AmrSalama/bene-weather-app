import axios from 'axios';
import { auth as authService } from '../service';
import { logout as logoutAction } from './auth';
import {
  CITY_LIST_FETCHED,
  CITY_LIST_LOADING,
  CITY_LIST_ERROR,
  CITY_SEARCH_FETCHED,
  CITY_SEARCH_LOADING,
  CITY_SEARCH_ERROR,
  CITY_ADD_REQUEST,
  CITY_ADD_SUCCESS,
  CITY_ADD_ERROR,
  CITY_RESET
} from '../constants';

const { REACT_APP_API_BASE: API_BASE } = process.env;

export const fetch = () => {
  return dispatch => {
    dispatch({ type: CITY_LIST_LOADING });
    axios
      .get(`${API_BASE}/cities`, {
        headers: { Authorization: `Bearer ${authService.getToken()}` }
      })
      .then(response => {
        dispatch({ type: CITY_LIST_FETCHED, cities: response.data });
      })
      .catch(err => {
        if (err.response.status === 401) {
          return dispatch(logoutAction()); // expired token case
        }

        const msg = 'Something went wrong, please try again later';
        dispatch({ type: CITY_LIST_ERROR, error: msg });
      });
  };
};

export const search = keyword => {
  return dispatch => {
    dispatch({ type: CITY_SEARCH_LOADING });
    axios
      .get(`${API_BASE}/cities/search?q=${keyword}&limit=8`, {
        headers: { Authorization: `Bearer ${authService.getToken()}` }
      })
      .then(response => {
        dispatch({ type: CITY_SEARCH_FETCHED, cities: response.data });
      })
      .catch(err => {
        if (err.response.status === 401) {
          return dispatch(logoutAction()); // expired token case
        }

        const msg = 'Something went wrong, please try again later';
        dispatch({ type: CITY_SEARCH_ERROR, error: msg });
      });
  };
};

export const startSearchLoading = () => ({ type: CITY_SEARCH_LOADING });

export const add = cityId => {
  return dispatch => {
    dispatch({ type: CITY_ADD_REQUEST });
    axios
      .post(
        `${API_BASE}/cities`,
        { cityId: cityId },
        { headers: { Authorization: `Bearer ${authService.getToken()}` } }
      )
      .then(() => {
        dispatch({ type: CITY_ADD_SUCCESS });
      })
      .catch(err => {
        if (err.response.status === 401) {
          return dispatch(logoutAction()); // expired token case
        }

        const msg = 'Something went wrong, please try again later';
        dispatch({ type: CITY_ADD_ERROR, error: msg });
      });
  };
};

export const reset = () => ({ type: CITY_RESET });
