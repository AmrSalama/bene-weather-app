import axios from 'axios';
import { auth as authService } from '../service';
import { AUTH_USER, UNAUTH_USER, AUTH_ERROR, AUTH_LOADING } from '../constants';

const { REACT_APP_API_BASE: API_BASE } = process.env;

export const login = ({ username, password }) => {
  return dispatch => {
    dispatch({ type: AUTH_LOADING });
    axios
      .post(`${API_BASE}/login`, { username, password })
      .then(response => {
        authService.setToken(response.data.token);
        dispatch({ type: AUTH_USER });
      })
      .catch(err => {
        const { status } = err.response;
        const msg =
          status === 401
            ? 'Invalid Username or Password'
            : 'Something went wrong, please try again later';

        dispatch({ type: AUTH_ERROR, error: msg });
      });
  };
};

export const logout = () => {
  authService.removeToken();
  return { type: UNAUTH_USER };
};
