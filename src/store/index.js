import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension/logOnlyInProduction';

import { auth } from '../service';
import { AUTH_USER } from '../constants';
import rootReducer from '../reducers';

const composeEnhancers = composeWithDevTools({});
const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(thunk))
);

// speical case: check for logged user
if (auth.getToken()) store.dispatch({ type: AUTH_USER });

export default store;
