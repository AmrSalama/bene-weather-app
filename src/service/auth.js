const TOKEN_LOCAL_STORAGE_NAME = 'bene-weather-token';

export const getToken = () => localStorage.getItem(TOKEN_LOCAL_STORAGE_NAME);

export const setToken = token => {
  localStorage.setItem(TOKEN_LOCAL_STORAGE_NAME, token);
};

export const removeToken = () => {
  localStorage.removeItem(TOKEN_LOCAL_STORAGE_NAME);
};
