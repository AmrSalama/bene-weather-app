export const dateDifference = (timezone, date) => {
  const UTC = date.getTime() + date.getTimezoneOffset() * 60000;
  return new Date(UTC + timezone * 60000);
};

export const twoDigitFormat = time => (time > 9 ? '' + time : '0' + time);
