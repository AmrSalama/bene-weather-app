import { combineReducers } from 'redux';
import { reducer as authReducer } from './auth';
import { reducer as citiesReducer } from './cities';
import { reducer as weatherReducer } from './weather';

const rootReducer = combineReducers({
  auth: authReducer,
  cities: citiesReducer,
  weather: weatherReducer
});

export default rootReducer;
