import { AUTH_USER, UNAUTH_USER, AUTH_ERROR, AUTH_LOADING } from '../constants';

const initalState = {
  authenticated: false,
  loading: false,
  error: null
};

export const reducer = (state = initalState, action) => {
  switch (action.type) {
    case AUTH_USER:
      return { ...state, authenticated: true, error: null, loading: false };

    case UNAUTH_USER:
      return { ...state, authenticated: false };

    case AUTH_ERROR:
      return { ...state, error: action.error, loading: false };

    case AUTH_LOADING:
      return { ...state, loading: true };

    default:
      return state;
  }
};
