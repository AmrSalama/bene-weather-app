import {
  CITY_LIST_FETCHED,
  CITY_LIST_LOADING,
  CITY_LIST_ERROR,
  CITY_SEARCH_FETCHED,
  CITY_SEARCH_LOADING,
  CITY_SEARCH_ERROR,
  CITY_ADD_REQUEST,
  CITY_ADD_SUCCESS,
  CITY_ADD_ERROR,
  CITY_RESET
} from '../constants';

const initalState = {
  list: [],
  loading: false,
  error: null,
  search: {
    list: [],
    loading: false,
    error: null
  },
  add: {
    loading: null,
    error: null
  }
};

export const reducer = (state = initalState, action) => {
  switch (action.type) {
    case CITY_LIST_FETCHED:
      return { ...state, list: action.cities, error: null, loading: false };

    case CITY_LIST_LOADING:
      return { ...state, loading: true };

    case CITY_LIST_ERROR:
      return { ...state, error: action.error, loading: false };

    case CITY_SEARCH_FETCHED:
      const fstate = { ...state };
      fstate.search = { ...fstate.search, list: action.cities, loading: false };
      return fstate;

    case CITY_SEARCH_LOADING:
      const lstate = { ...state };
      lstate.search = { ...lstate.search, error: null, loading: true };
      return lstate;

    case CITY_SEARCH_ERROR:
      const estate = { ...state };
      estate.search = { ...estate.search, error: action.error };
      return estate;

    case CITY_ADD_REQUEST:
      const arstate = { ...state };
      arstate.add = { ...arstate.search, error: null, loading: true };
      return arstate;

    case CITY_ADD_SUCCESS:
      const asstate = { ...state };
      asstate.add = { ...asstate.search, error: null, loading: false };
      return asstate;

    case CITY_ADD_ERROR:
      const aestate = { ...state };
      aestate.add = { ...aestate.search, error: action.error, loading: false };
      return aestate;

    case CITY_RESET:
      const astat = { ...state };
      astat.add = { ...astat.search, error: null, loading: null };
      astat.search = { ...astat.search, error: null, loading: null, list: [] };
      return astat;

    default:
      return state;
  }
};
