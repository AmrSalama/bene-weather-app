import {
  CITY_WEATHER_FETCHED,
  CITY_WEATHER_LOADING,
  CITY_WEATHER_ERROR,
  CITY_WEATHER_RESET
} from '../constants';

const initalState = {
  details: {},
  loading: false,
  error: null
};

export const reducer = (state = initalState, action) => {
  switch (action.type) {
    case CITY_WEATHER_FETCHED:
      return { ...state, details: action.weather, error: null, loading: false };

    case CITY_WEATHER_LOADING:
      return { ...state, error: null, loading: true };

    case CITY_WEATHER_ERROR:
      return { ...state, error: action.error, loading: false };

    case CITY_WEATHER_RESET:
      return { ...state, error: null, loading: false };

    default:
      return state;
  }
};
