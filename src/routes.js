import React from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';

import App from './components/App';
import RequireAuth from './components/RequireAuth';
import Login from './components/Login';
import Cities from './components/Cities';
import Weather from './components/Weather';
import Search from './components/Search';

const Routes = () => {
  return (
    <App>
      <Switch>
        <Route path="/login" component={Login} />
        <Route path="/cities/new" component={RequireAuth(Search)} />
        <Route path="/cities/:id" component={RequireAuth(Weather)} />
        <Route path="/cities" component={RequireAuth(Cities)} />
        <Redirect from="/" to="cities" />
        <Redirect to="/" />
      </Switch>
    </App>
  );
};

export default Routes;
