import React, { Component } from 'react';
import { connect } from 'react-redux';
import { cities } from '../actions';
import Spinner from './common/Spinner';
import './Cities.scss';

class Cities extends Component {
  componentDidMount() {
    this.props.fetchCities();
  }

  printMessage = () => {
    const { loading, error, cities } = this.props;
    if (!loading) {
      if (error)
        return (
          <p className="Cities__message Cities__message--error">{error}</p>
        );
      else if (cities.length === 0)
        return <p className="Cities__message">Empty List</p>;
    }
  };

  goTo = path => this.props.history.push(path);

  render() {
    const { loading, cities } = this.props;

    return (
      <div className="Cities">
        {loading && (
          <div className="Cities__loader">
            <Spinner />
          </div>
        )}

        {this.printMessage()}

        {cities.map(city => (
          <button
            key={city.id}
            onClick={() => this.goTo(`/cities/${city.id}`)}
            className="Cities__city-btn"
          >
            {city.name}
          </button>
        ))}

        <button
          className="Cities__add-btn"
          onClick={() => this.goTo('/cities/new')}
        >
          +
        </button>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  cities: state.cities.list,
  error: state.cities.error,
  loading: state.cities.loading
});

const mapDispatchToProps = dispatch => ({
  fetchCities: () => dispatch(cities.fetch())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Cities);
