import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { dateDifference, twoDigitFormat } from '../utils';
import { weather } from '../actions';
import Spinner from './common/Spinner';
import Back from './common/Back';
import WeatherIcon from './common/WeatherIcon';
import './Weather.scss';

class Weather extends Component {
  constructor(props) {
    super(props);
    this.state = { date: new Date() };
  }

  componentDidMount() {
    const { id: cityId } = this.props.match.params;
    this.timer = setInterval(() => {
      this.setState({ date: new Date() });
    }, 1000);
    this.props.fetchWeather(cityId);
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  render() {
    const { date } = this.state;
    const { loading, weather, error, history } = this.props;
    const { timezone, sunrise, sunset, description, weatherId, temp } = weather;

    if (error) {
      this.props.reset();
      return <Redirect to="/cities" />;
    }

    const currentDate = dateDifference(timezone / 60, date);
    const sunriseDate = dateDifference(timezone / 60, new Date(sunrise * 1000));
    const sunsetDate = dateDifference(timezone / 60, new Date(sunset * 1000));
    const currentTimestamp = Math.round(currentDate.getTime() / 1000);

    return (
      <div className="Weather">
        <Back onClick={() => history.push('/cities')} />

        {loading && (
          <div className="Weather__loader">
            <Spinner />
          </div>
        )}

        {!loading && weatherId && (
          <React.Fragment>
            <div className="Weather__desc">
              <span className="Weather__desc__icon">
                <WeatherIcon
                  currentTimestamp={currentTimestamp}
                  sunsetTimestamp={sunset}
                  weatherId={weatherId}
                />
              </span>
              <p className="Weather__desc__details">{description}</p>
            </div>

            <div className="Weather__city">
              <p className="Weather__city__current-time">
                {twoDigitFormat(currentDate.getHours())}
              </p>
              <p className="Weather__city__current-time">
                {twoDigitFormat(currentDate.getMinutes())}
              </p>
              <p className="Weather__city__name">{weather.name}</p>
            </div>

            <div className="Weather__temp">
              <p>
                <i className="wi wi-thermometer" />
                <span>
                  {Math.round(temp) + ' '}
                  <sup>o</sup>C
                </span>
              </p>
              <p>
                <i className="wi wi-sunrise" />
                <span>{`${twoDigitFormat(
                  sunriseDate.getHours()
                )}:${twoDigitFormat(sunriseDate.getHours())}`}</span>
              </p>
              <p>
                <i className="wi wi-sunset" />
                <span>{`${twoDigitFormat(
                  sunsetDate.getHours()
                )}:${twoDigitFormat(sunsetDate.getHours())}`}</span>
              </p>
            </div>
          </React.Fragment>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  weather: state.weather.details,
  loading: state.weather.loading,
  error: state.weather.error
});

const mapDispatchToProps = dispatch => ({
  fetchWeather: cityId => dispatch(weather.fetch(cityId)),
  reset: () => dispatch(weather.reset())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Weather);
