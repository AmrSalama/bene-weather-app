import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

// Require Auth HOC
export default ComposedComponent => {
  const UNAUTH_REDIRECT_PATH = '/login';

  class Authentication extends Component {
    componentWillMount() {
      if (!this.props.authenticated) {
        this.props.history.replace(UNAUTH_REDIRECT_PATH);
      }
    }

    componentWillUpdate(nextProps) {
      if (!nextProps.authenticated) {
        this.props.history.replace(UNAUTH_REDIRECT_PATH);
      }
    }

    render() {
      const { authenticated } = this.props;
      return authenticated ? <ComposedComponent {...this.props} /> : null;
    }
  }

  Authentication.propTypes = {
    router: PropTypes.object
  };

  const mapStateToProps = state => ({
    authenticated: state.auth.authenticated
  });

  return connect(mapStateToProps)(Authentication);
};
