import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import reactStringReplace from 'react-string-replace';
import { cities } from '../actions';
import Input from './common/Input';
import Button from './common/Button';
import Back from './common/Back';
import './Search.scss';

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = { keyword: '', selectedCity: null };
  }

  handleChange = ({ currentTarget }) => {
    const { name, value } = currentTarget;
    this.setState({ ...this.state, selectedCity: null, [name]: value });

    if (this.timeout) clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      this.props.fetchSearch(value);
    }, 1000);
    this.props.startSearchLoading();
  };

  handleSelect = city => {
    this.setState({ selectedCity: city, keyword: city.name });
    this.props.fetchSearch('');
  };

  handleSave = () => {
    const { selectedCity } = this.state;
    if (!selectedCity) return;
    this.props.addCity(selectedCity.id);
  };

  handleBackClick = () => {
    const { history, addReset } = this.props;
    addReset();
    history.push('/cities');
  };

  render() {
    const { keyword, selectedCity } = this.state;
    const { cities, searchLoading, addLoading, addError } = this.props;

    if (addLoading === false && !addError) {
      this.props.addReset();
      return <Redirect to="/cities" />;
    }

    return (
      <div className="Search">
        <Back onClick={this.handleBackClick} />
        <Input
          value={keyword}
          name="keyword"
          placeholder="Search for city ..."
          loading={searchLoading}
          onChange={this.handleChange}
        />
        <ul className="Search__suggestions">
          {cities.map(city => (
            <li
              key={city.id}
              className="Search__suggestions__item"
              onClick={() => this.handleSelect(city)}
            >
              {reactStringReplace(city.name, keyword, (match, i) => (
                <span key={i} className="Search__suggestions__item__match">
                  {match}
                </span>
              ))}
            </li>
          ))}
        </ul>
        {selectedCity && <Button label={'Save'} onClick={this.handleSave} />}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  searchLoading: state.cities.search.loading,
  searchError: state.cities.search.error,
  cities: state.cities.search.list,
  addLoading: state.cities.add.loading,
  addError: state.cities.add.error
});

const mapDispatchToProps = dispatch => ({
  fetchSearch: q => dispatch(cities.search(q)),
  startSearchLoading: () => dispatch(cities.startSearchLoading()),
  addCity: cityId => dispatch(cities.add(cityId)),
  addReset: () => dispatch(cities.reset())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Search);
