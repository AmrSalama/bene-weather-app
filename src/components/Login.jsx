import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { auth } from '../actions';

import Input from './common/Input';
import Button from './common/Button';
import Spinner from './common/Spinner';
import './Login.scss';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = { username: '', password: '' };
  }

  handleChange = ({ currentTarget }) => {
    const { name, value } = currentTarget;
    this.setState({ ...this.state, [name]: value });
  };

  handleSubmit = e => {
    e.preventDefault();

    const { username, password } = this.state;
    this.props.login({ username, password });
  };

  render() {
    const { username, password } = this.state;
    const { authenticated, loading, error } = this.props;

    return (
      <React.Fragment>
        {authenticated && <Redirect to="/" />}
        <div className="Login">
          <form
            className={'Login__form' + (loading ? ' Login__form--loading' : '')}
            autoComplete="off"
            onSubmit={this.handleSubmit}
          >
            <div className="Login__form__overlay" />

            <Input
              name="username"
              label="Username"
              value={username}
              onChange={this.handleChange}
            />

            <Input
              type="password"
              name="password"
              label="Password"
              value={password}
              onChange={this.handleChange}
            />

            <Button label="Login" />

            <div className="Login__form__loader">
              <Spinner />
            </div>

            {error && <div className="Login__form__error">{error}</div>}
          </form>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  authenticated: state.auth.authenticated,
  loading: state.auth.loading,
  error: state.auth.error
});

const mapDispatchToProps = dispatch => ({
  login: data => dispatch(auth.login(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
