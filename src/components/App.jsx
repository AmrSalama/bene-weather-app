import React from 'react';
import './App.scss';

function App(props) {
  return (
    <div className="App">
      <div className="App__container">{props.children}</div>
    </div>
  );
}

export default App;
