import React from 'react';
import PropTypes from 'prop-types';
import Spinner from './Spinner';
import './Input.scss';

const Input = props => {
  const { loading, type, label, placeholder, value, name, onChange } = props;

  return (
    <div className="InputGroup">
      <input
        className="InputGroup__input"
        type={type}
        value={value}
        name={name}
        onChange={onChange}
        autoComplete="off"
        placeholder={placeholder}
        required
      />
      <span className="InputGroup__bar" />
      <label className="InputGroup__label">{label}</label>

      {loading && (
        <div className="InputGroup__spinner">
          <Spinner />
        </div>
      )}
    </div>
  );
};

Input.propTypes = {
  loading: PropTypes.bool,
  type: PropTypes.string,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  value: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired
};

Input.defaultProps = { type: 'text' };

export default Input;
