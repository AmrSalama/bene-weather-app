import React from 'react';
import { IoIosArrowForward } from 'react-icons/io';
import PropTypes from 'prop-types';
import './Back.scss';

const Back = ({ onClick }) => {
  return (
    <button className="Back" onClick={onClick}>
      <IoIosArrowForward />
    </button>
  );
};

Back.propTypes = {
  onClick: PropTypes.func
};

export default Back;
