import React from 'react';
import PropTypes from 'prop-types';

const WeatherIcon = ({ currentTimestamp, sunsetTimestamp, weatherId }) => {
  return (
    <i
      className={`wi wi-owm-${
        currentTimestamp < sunsetTimestamp ? 'day' : 'night'
      }-${weatherId}`}
    />
  );
};

WeatherIcon.propTypes = {
  currentTimestamp: PropTypes.number.isRequired,
  sunsetTimestamp: PropTypes.number.isRequired,
  weatherId: PropTypes.number.isRequired
};

export default WeatherIcon;
